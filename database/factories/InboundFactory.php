<?php

$factory->define(App\Inbound::class, function (Faker\Generator $faker) {
    return [
        "ref_no" => $faker->name,
        "company_name" => $faker->name,
        "type" => collect(["DOCUMENT","PARCEL","SPECIAL",])->random(),
        "consignment" => $faker->name,
        "weight" => $faker->randomFloat(2, 1, 100),
        "no_of_pieces" => $faker->randomNumber(2),
        "remarks" => $faker->name,
    ];
});
