<?php

$factory->define(App\Booking::class, function (Faker\Generator $faker) {
    return [
        "date" => $faker->date("Y-m-d H:i:s", $max = 'now'),
        "consignment_no" => $faker->name,
        "consigner_name" => $faker->name,
        "consigner_mobile_no" => $faker->name,
        "from" => $faker->name,
        "to" => $faker->name,
        "consignee_name" => $faker->name,
        "consignee_mobile_no" => $faker->name,
        "service_type" => collect(["normal","priority","in 6 hrs",])->random(),
        "weight" => $faker->randomNumber(2),
        "dox" => collect(["1","2",])->random(),
        "amount" => $faker->randomNumber(2),
        "status" => $faker->name,
        "shipment_status" => collect(["shipped","in_transit","delivered",])->random(),
        "descvd" => $faker->name,
    ];
});
