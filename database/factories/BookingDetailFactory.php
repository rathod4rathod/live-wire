<?php

$factory->define(App\BookingDetail::class, function (Faker\Generator $faker) {
    return [
        "booking_id" => factory('App\Booking')->create(),
        "location" => $faker->name,
        "details" => $faker->name,
        "date" => $faker->date("Y-m-d", $max = 'now'),
        "time" => $faker->date("H:i:s", $max = 'now'),
    ];
});
