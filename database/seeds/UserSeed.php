<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'name' => 'Admin', 'email' => 'admin@admin.com', 'password' => '$2y$10$7UexZJApnFhnTq/gX89jPOK2N9ASSOdmU1j7HFTWtv4pY9Gen5VIK', 'role_id' => 1, 'remember_token' => '',],
            ['id' => 2, 'name' => 'test', 'email' => 'test@gmail.com', 'password' => '$2y$10$F7WFc6u9a1w75bvKrZrIb.62zb/5uqNSVr2HpWnQik5NB60QYxFMK', 'role_id' => 2, 'remember_token' => null,],

        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
