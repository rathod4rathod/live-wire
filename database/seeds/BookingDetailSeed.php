<?php

use Illuminate\Database\Seeder;

class BookingDetailSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'booking_id' => 1, 'location' => 'surart', 'details' => 'nhjj hjghj ghjg h ghj', 'date' => '2019-04-19', 'time' => '',],
            ['id' => 2, 'booking_id' => 1, 'location' => 'bardoli', 'details' => 'ddddddd', 'date' => '2019-05-11', 'time' => '',],
            ['id' => 3, 'booking_id' => 2, 'location' => 'ssss', 'details' => 'wwwww', 'date' => '2019-05-01', 'time' => '19:07:53',],
            ['id' => 4, 'booking_id' => 2, 'location' => 'hhhh', 'details' => 'fffff', 'date' => '2019-05-01', 'time' => '19:08:03',],

        ];

        foreach ($items as $item) {
            \App\BookingDetail::create($item);
        }
    }
}
