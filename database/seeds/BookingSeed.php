<?php

use Illuminate\Database\Seeder;

class BookingSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'date' => '2019-04-12 11:04:59', 'consignment_no' => 'E64353', 'consigner_name' => '345345', 'consigner_mobile_no' => '345345345435', 'from' => 'SURAT', 'to' => 'AMDAVAD', 'consignee_name' => 'DILIP', 'consignee_mobile_no' => '44444444', 'service_type' => 'in 6 hrs', 'weight' => 203, 'dox' => '1', 'amount' => 888, 'status' => null, 'shipment_status' => null, 'pod' => null,],
            ['id' => 2, 'date' => '2019-05-01 00:13:53', 'consignment_no' => '123456', 'consigner_name' => 'testtting123456', 'consigner_mobile_no' => '9887745421', 'from' => 'dubai', 'to' => 'landon', 'consignee_name' => 'mabuli', 'consignee_mobile_no' => '8754875421', 'service_type' => 'priority', 'weight' => 6, 'dox' => '1', 'amount' => 12, 'status' => 'nothing', 'shipment_status' => 'shipped', 'pod' => '/tmp/phpt9vmX0',],

        ];

        foreach ($items as $item) {
            \App\Booking::create($item);
        }
    }
}
