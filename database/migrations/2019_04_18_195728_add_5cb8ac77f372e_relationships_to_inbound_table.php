<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5cb8ac77f372eRelationshipsToInboundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inbounds', function(Blueprint $table) {
            if (!Schema::hasColumn('inbounds', 'from_id')) {
                $table->integer('from_id')->unsigned()->nullable();
                $table->foreign('from_id', '293822_5cb8ac74bd7c8')->references('id')->on('cities')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inbounds', function(Blueprint $table) {
            
        });
    }
}
