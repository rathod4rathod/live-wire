<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1555606644InboundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inbounds', function (Blueprint $table) {
            
if (!Schema::hasColumn('inbounds', 'type')) {
                $table->enum('type', array('DOCUMENT', 'PARCEL', 'SPECIAL'))->nullable();
                }
if (!Schema::hasColumn('inbounds', 'consignment')) {
                $table->string('consignment')->nullable();
                }
if (!Schema::hasColumn('inbounds', 'weight')) {
                $table->double('weight', 4, 3)->nullable();
                }
if (!Schema::hasColumn('inbounds', 'no_of_pieces')) {
                $table->integer('no_of_pieces')->nullable()->unsigned();
                }
if (!Schema::hasColumn('inbounds', 'remarks')) {
                $table->string('remarks')->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inbounds', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('consignment');
            $table->dropColumn('weight');
            $table->dropColumn('no_of_pieces');
            $table->dropColumn('remarks');
            
        });

    }
}
