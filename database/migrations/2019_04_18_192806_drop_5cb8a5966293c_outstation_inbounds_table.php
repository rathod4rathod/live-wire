<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drop5cb8a5966293cOutstationInboundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('outstation_inbounds');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(! Schema::hasTable('outstation_inbounds')) {
            Schema::create('outstation_inbounds', function (Blueprint $table) {
                $table->increments('id');
                $table->string('ref_no')->nullable();
                $table->string('company_name')->nullable();
                $table->string('from')->nullable();
                $table->enum('type', array('DOCUMENT', 'PARCEL', 'SPECIAL'));
                $table->string('consignment')->nullable();
                $table->double('weight', 4, 3)->nullable();
                $table->integer('no_of_pieces')->nullable()->unsigned();
                $table->string('remarks')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

            $table->index(['deleted_at']);
            });
        }
    }
}
