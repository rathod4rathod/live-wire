<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1555828471InboundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inbounds', function (Blueprint $table) {
            if(Schema::hasColumn('inbounds', 'from_id')) {
                $table->dropForeign('293822_5cb8ac74bd7c8');
                $table->dropIndex('293822_5cb8ac74bd7c8');
                $table->dropColumn('from_id');
            }
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inbounds', function (Blueprint $table) {
                        
        });

    }
}
