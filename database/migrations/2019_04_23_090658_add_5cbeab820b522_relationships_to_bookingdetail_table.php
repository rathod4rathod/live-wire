<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5cbeab820b522RelationshipsToBookingDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_details', function(Blueprint $table) {
            if (!Schema::hasColumn('booking_details', 'booking_id')) {
                $table->integer('booking_id')->unsigned()->nullable();
                $table->foreign('booking_id', '295627_5cbea6baaab45')->references('id')->on('bookings')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_details', function(Blueprint $table) {
            
        });
    }
}
