<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1554997760BookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('bookings')) {
            Schema::create('bookings', function (Blueprint $table) {
                $table->increments('id');
                $table->datetime('date')->nullable();
                $table->string('consignment_no')->nullable();
                $table->string('consigner_name')->nullable();
                $table->string('consigner_mobile_no')->nullable();
                $table->string('from')->nullable();
                $table->string('to')->nullable();
                $table->string('consignee_name')->nullable();
                $table->string('consignee_mobile_no')->nullable();
                $table->enum('service_type', array('normal', 'priority', 'in 6 hrs'))->nullable();
                $table->integer('weight')->nullable()->unsigned();
                $table->string('dox')->nullable();
                $table->integer('amount')->nullable()->unsigned();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
