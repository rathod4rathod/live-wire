<?php
namespace App\Http\Livewire;

use Livewire\Component;
use App\BookingDetail;

class AddBooking extends Component
{
	public $booking_id;
	public $location;
	public $details;
	public $date;
	public $time;
	public $type = 'add';
	public $myid = 0;
    protected $listeners = ['mybookingid'];
	public function mybookingid($mybookingid)
   {
   		$data = \App\BookingDetail::where('id',$mybookingid)->first();
   		$this->booking_id = $data->booking_id;
		$this->location = $data->location;
		$this->details = $data->details;
		$this->date = $data->date;
		$this->time = $data->time;
		$this->myid = $data->id;
		$this->type = 'edit';
   }
	public function addBooking(){

		  $this->validate([
            'booking_id' => 'required|numeric|gt:0',
            'location' => 'required',
            'details' => 'required',
            'date' => 'required',
            'time' => 'required|date_format:H:i:s',
        ]);
		if($this->type == 'edit'){
			$data = \App\BookingDetail::where('id',$this->myid)->update([
				'booking_id' => $this->booking_id,
				'location'	=> $this->location,
				'details'	=> $this->details,
				'date'	=> date('Y-m-d',strtotime(str_replace('/', '-', $this->date))),
				'time'	=> $this->time
			]);
		}else{
			$data = new \App\BookingDetail([
				'booking_id' => $this->booking_id,
				'location'	=> $this->location,
				'details'	=> $this->details,
				'date'	=> date('Y-m-d',strtotime(str_replace('/', '-', $this->date))),
				'time'	=> $this->time
			]);
			$data->save();
		}
		$tt = ($this->type && $this->type == 'edit')?' update ':' add';
		$this->booking_id = '';
		$this->location = '';
		$this->details = '';
		$this->date = '';
		$this->time = '';
		$this->type = 'add';
		$this->emit('refreshData');
		 $this->emit('alert', ['type' => 'success', 'message' => 'Data '.$tt.' Successfully']);
	}
	public function refreshData(){
    	$this->render();
    }
    public function render()
    {
        return view('livewire.add-booking',[
        	'bookingList'=>\App\Booking::get()
        ]);
    }
}
