<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EditBooking extends Component
{
    public function render()
    {
        return view('livewire.edit-booking');
    }
}
