<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;

class Booking extends Component
{
	 use WithPagination;
	 public $search;
     protected $listeners = ['refreshData','editBooking'];
    public function editBooking($bookingid){
    	$this->emit('mybookingid', $bookingid);
    }
    public function deleteBooking($bookingid){
    	$data = \App\BookingDetail::where('id',$bookingid)->delete();
        if($data)
        {
             session()->flash('success1', 'Booking Detail deleted successfully.');
             $this->emit('alert', ['type' => 'success', 'message' => 'Booking Detail deleted successfully.']);

        }else{
             session()->flash('error1', 'Somethink went wrong.');
             $this->emit('alert', ['type' => 'error', 'message' => 'Somethink went wrong.']);

        }
    	$this->refreshData();
        $this->emit('refreshData');

    }
    public function refreshData(){
    	$this->render();
    }
    public function render()
    {
        return view('livewire.booking',[
        	'booking' => \App\BookingDetail::where('details','like','%'.$this->search.'%')->orWhere('location','like','%'.$this->search.'%')->paginate(10)
    	]);
    }
}
