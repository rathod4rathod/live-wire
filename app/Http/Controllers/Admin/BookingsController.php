<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBookingsRequest;
use App\Http\Requests\Admin\UpdateBookingsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\DataTables\DataTables;

class BookingsController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Booking.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('booking_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Booking::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('booking_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'bookings.id',
                'bookings.date',
                'bookings.consignment_no',
                'bookings.consigner_name',
                'bookings.consigner_mobile_no',
                'bookings.from',
                'bookings.to',
                'bookings.consignee_name',
                'bookings.consignee_mobile_no',
                'bookings.service_type',
                'bookings.weight',
                'bookings.dox',
                'bookings.amount',
                'bookings.status',
                'bookings.shipment_status',
                'bookings.pod',
                'bookings.descvd',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'booking_';
                $routeKey = 'admin.bookings';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('date', function ($row) {
                return $row->date ? $row->date : '';
            });
            $table->editColumn('consignment_no', function ($row) {
                return $row->consignment_no ? $row->consignment_no : '';
            });
            $table->editColumn('consigner_name', function ($row) {
                return $row->consigner_name ? $row->consigner_name : '';
            });
            $table->editColumn('consigner_mobile_no', function ($row) {
                return $row->consigner_mobile_no ? $row->consigner_mobile_no : '';
            });
            $table->editColumn('from', function ($row) {
                return $row->from ? $row->from : '';
            });
            $table->editColumn('to', function ($row) {
                return $row->to ? $row->to : '';
            });
            $table->editColumn('consignee_name', function ($row) {
                return $row->consignee_name ? $row->consignee_name : '';
            });
            $table->editColumn('consignee_mobile_no', function ($row) {
                return $row->consignee_mobile_no ? $row->consignee_mobile_no : '';
            });
            $table->editColumn('service_type', function ($row) {
                return $row->service_type ? $row->service_type : '';
            });
            $table->editColumn('weight', function ($row) {
                return $row->weight ? $row->weight : '';
            });
            $table->editColumn('dox', function ($row) {
                return $row->dox ? $row->dox : '';
            });
            $table->editColumn('amount', function ($row) {
                return $row->amount ? $row->amount : '';
            });
            $table->editColumn('status', function ($row) {
                return $row->status ? $row->status : '';
            });
            $table->editColumn('shipment_status', function ($row) {
                return $row->shipment_status ? $row->shipment_status : '';
            });
            $table->editColumn('pod', function ($row) {
                if($row->pod) { return '<a href="'.asset(env('UPLOAD_PATH').'/'.$row->pod) .'" target="_blank">Download file</a>'; };
            });
            $table->editColumn('descvd', function ($row) {
                return $row->descvd ? $row->descvd : '';
            });

            $table->rawColumns(['actions','massDelete','pod']);

            return $table->make(true);
        }

        return view('admin.bookings.index');
    }

    /**
     * Show the form for creating new Booking.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('booking_create')) {
            return abort(401);
        }        $enum_service_type = Booking::$enum_service_type;
                    $enum_shipment_status = Booking::$enum_shipment_status;
            
        return view('admin.bookings.create', compact('enum_service_type', 'enum_shipment_status'));
    }

    /**
     * Store a newly created Booking in storage.
     *
     * @param  \App\Http\Requests\StoreBookingsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookingsRequest $request)
    {
        if (! Gate::allows('booking_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $booking = Booking::create($request->all());



        return redirect()->route('admin.bookings.index');
    }


    /**
     * Show the form for editing Booking.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('booking_edit')) {
            return abort(401);
        }        $enum_service_type = Booking::$enum_service_type;
                    $enum_shipment_status = Booking::$enum_shipment_status;
            
        $booking = Booking::findOrFail($id);

        return view('admin.bookings.edit', compact('booking', 'enum_service_type', 'enum_shipment_status'));
    }

    /**
     * Update Booking in storage.
     *
     * @param  \App\Http\Requests\UpdateBookingsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookingsRequest $request, $id)
    {
        if (! Gate::allows('booking_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $booking = Booking::findOrFail($id);
        $booking->update($request->all());



        return redirect()->route('admin.bookings.index');
    }


    /**
     * Display Booking.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('booking_view')) {
            return abort(401);
        }
        $booking_details = \App\BookingDetail::where('booking_id', $id)->get();

        $booking = Booking::findOrFail($id);

        return view('admin.bookings.show', compact('booking', 'booking_details'));
    }


    /**
     * Remove Booking from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('booking_delete')) {
            return abort(401);
        }
        $booking = Booking::findOrFail($id);
        $booking->delete();

        return redirect()->route('admin.bookings.index');
    }

    /**
     * Delete all selected Booking at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('booking_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Booking::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Booking from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('booking_delete')) {
            return abort(401);
        }
        $booking = Booking::onlyTrashed()->findOrFail($id);
        $booking->restore();

        return redirect()->route('admin.bookings.index');
    }

    /**
     * Permanently delete Booking from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('booking_delete')) {
            return abort(401);
        }
        $booking = Booking::onlyTrashed()->findOrFail($id);
        $booking->forceDelete();

        return redirect()->route('admin.bookings.index');
    }
}
