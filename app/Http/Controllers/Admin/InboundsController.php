<?php

namespace App\Http\Controllers\Admin;

use App\Inbound;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreInboundsRequest;
use App\Http\Requests\Admin\UpdateInboundsRequest;
use Yajra\DataTables\DataTables;

class InboundsController extends Controller
{
    /**
     * Display a listing of Inbound.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('inbound_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Inbound::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('inbound_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'inbounds.id',
                'inbounds.ref_no',
                'inbounds.company_name',
                'inbounds.type',
                'inbounds.consignment',
                'inbounds.weight',
                'inbounds.no_of_pieces',
                'inbounds.remarks',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'inbound_';
                $routeKey = 'admin.inbounds';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('ref_no', function ($row) {
                return $row->ref_no ? $row->ref_no : '';
            });
            $table->editColumn('company_name', function ($row) {
                return $row->company_name ? $row->company_name : '';
            });
            $table->editColumn('type', function ($row) {
                return $row->type ? $row->type : '';
            });
            $table->editColumn('consignment', function ($row) {
                return $row->consignment ? $row->consignment : '';
            });
            $table->editColumn('weight', function ($row) {
                return $row->weight ? $row->weight : '';
            });
            $table->editColumn('no_of_pieces', function ($row) {
                return $row->no_of_pieces ? $row->no_of_pieces : '';
            });
            $table->editColumn('remarks', function ($row) {
                return $row->remarks ? $row->remarks : '';
            });

            $table->rawColumns(['actions','massDelete']);

            return $table->make(true);
        }

        return view('admin.inbounds.index');
    }

    /**
     * Show the form for creating new Inbound.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('inbound_create')) {
            return abort(401);
        }        $enum_type = Inbound::$enum_type;
            
        return view('admin.inbounds.create', compact('enum_type'));
    }

    /**
     * Store a newly created Inbound in storage.
     *
     * @param  \App\Http\Requests\StoreInboundsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInboundsRequest $request)
    {
        if (! Gate::allows('inbound_create')) {
            return abort(401);
        }
        $inbound = Inbound::create($request->all());



        return redirect()->route('admin.inbounds.index');
    }


    /**
     * Show the form for editing Inbound.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('inbound_edit')) {
            return abort(401);
        }        $enum_type = Inbound::$enum_type;
            
        $inbound = Inbound::findOrFail($id);

        return view('admin.inbounds.edit', compact('inbound', 'enum_type'));
    }

    /**
     * Update Inbound in storage.
     *
     * @param  \App\Http\Requests\UpdateInboundsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInboundsRequest $request, $id)
    {
        if (! Gate::allows('inbound_edit')) {
            return abort(401);
        }
        $inbound = Inbound::findOrFail($id);
        $inbound->update($request->all());



        return redirect()->route('admin.inbounds.index');
    }


    /**
     * Display Inbound.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('inbound_view')) {
            return abort(401);
        }
        $inbound = Inbound::findOrFail($id);

        return view('admin.inbounds.show', compact('inbound'));
    }


    /**
     * Remove Inbound from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('inbound_delete')) {
            return abort(401);
        }
        $inbound = Inbound::findOrFail($id);
        $inbound->delete();

        return redirect()->route('admin.inbounds.index');
    }

    /**
     * Delete all selected Inbound at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('inbound_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Inbound::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Inbound from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('inbound_delete')) {
            return abort(401);
        }
        $inbound = Inbound::onlyTrashed()->findOrFail($id);
        $inbound->restore();

        return redirect()->route('admin.inbounds.index');
    }

    /**
     * Permanently delete Inbound from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('inbound_delete')) {
            return abort(401);
        }
        $inbound = Inbound::onlyTrashed()->findOrFail($id);
        $inbound->forceDelete();

        return redirect()->route('admin.inbounds.index');
    }
}
