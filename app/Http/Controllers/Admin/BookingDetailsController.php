<?php

namespace App\Http\Controllers\Admin;

use App\BookingDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBookingDetailsRequest;
use App\Http\Requests\Admin\UpdateBookingDetailsRequest;
use Yajra\DataTables\DataTables;

class BookingDetailsController extends Controller
{
    /**
     * Display a listing of BookingDetail.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('booking_detail_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = BookingDetail::query();
            $query->with("booking");
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('booking_detail_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'booking_details.id',
                'booking_details.booking_id',
                'booking_details.location',
                'booking_details.details',
                'booking_details.date',
                'booking_details.time',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'booking_detail_';
                $routeKey = 'admin.booking_details';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('booking.consignment_no', function ($row) {
                return $row->booking ? $row->booking->consignment_no : '';
            });
            $table->editColumn('location', function ($row) {
                return $row->location ? $row->location : '';
            });
            $table->editColumn('details', function ($row) {
                return $row->details ? $row->details : '';
            });
            $table->editColumn('date', function ($row) {
                return $row->date ? $row->date : '';
            });
            $table->editColumn('time', function ($row) {
                return $row->time ? $row->time : '';
            });

            $table->rawColumns(['actions','massDelete']);

            return $table->make(true);
        }

        return view('admin.booking_details.index');
    }

    /**
     * Show the form for creating new BookingDetail.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('booking_detail_create')) {
            return abort(401);
        }
        
        $bookings = \App\Booking::get()->pluck('consignment_no', 'id')->prepend(trans('quickadmin.qa_please_select'), '');

        return view('admin.booking_details.create', compact('bookings'));
    }

    /**
     * Store a newly created BookingDetail in storage.
     *
     * @param  \App\Http\Requests\StoreBookingDetailsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookingDetailsRequest $request)
    {
        if (! Gate::allows('booking_detail_create')) {
            return abort(401);
        }
        $booking_detail = BookingDetail::create($request->all());



        return redirect()->route('admin.booking_details.index');
    }


    /**
     * Show the form for editing BookingDetail.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('booking_detail_edit')) {
            return abort(401);
        }
        
        $bookings = \App\Booking::get()->pluck('consignment_no', 'id')->prepend(trans('quickadmin.qa_please_select'), '');

        $booking_detail = BookingDetail::findOrFail($id);

        return view('admin.booking_details.edit', compact('booking_detail', 'bookings'));
    }

    /**
     * Update BookingDetail in storage.
     *
     * @param  \App\Http\Requests\UpdateBookingDetailsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookingDetailsRequest $request, $id)
    {
        if (! Gate::allows('booking_detail_edit')) {
            return abort(401);
        }
        $booking_detail = BookingDetail::findOrFail($id);
        $booking_detail->update($request->all());



        return redirect()->route('admin.booking_details.index');
    }


    /**
     * Display BookingDetail.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('booking_detail_view')) {
            return abort(401);
        }
        $booking_detail = BookingDetail::findOrFail($id);

        return view('admin.booking_details.show', compact('booking_detail'));
    }


    /**
     * Remove BookingDetail from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('booking_detail_delete')) {
            return abort(401);
        }
        $booking_detail = BookingDetail::findOrFail($id);
        $booking_detail->delete();

        return redirect()->route('admin.booking_details.index');
    }

    /**
     * Delete all selected BookingDetail at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('booking_detail_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = BookingDetail::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore BookingDetail from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('booking_detail_delete')) {
            return abort(401);
        }
        $booking_detail = BookingDetail::onlyTrashed()->findOrFail($id);
        $booking_detail->restore();

        return redirect()->route('admin.booking_details.index');
    }

    /**
     * Permanently delete BookingDetail from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('booking_detail_delete')) {
            return abort(401);
        }
        $booking_detail = BookingDetail::onlyTrashed()->findOrFail($id);
        $booking_detail->forceDelete();

        return redirect()->route('admin.booking_details.index');
    }
}
