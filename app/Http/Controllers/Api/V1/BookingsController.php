<?php

namespace App\Http\Controllers\Api\V1;

use App\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBookingsRequest;
use App\Http\Requests\Admin\UpdateBookingsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\DataTables\DataTables;

class BookingsController extends Controller
{
    use FileUploadTrait;

    public function index()
    {
        return Booking::all();
    }

    public function show($id)
    {
        return Booking::findOrFail($id);
    }

    public function update(UpdateBookingsRequest $request, $id)
    {
        $request = $this->saveFiles($request);
        $booking = Booking::findOrFail($id);
        $booking->update($request->all());
        

        return $booking;
    }

    public function store(StoreBookingsRequest $request)
    {
        $request = $this->saveFiles($request);
        $booking = Booking::create($request->all());
        

        return $booking;
    }

    public function destroy($id)
    {
        $booking = Booking::findOrFail($id);
        $booking->delete();
        return '';
    }
}
