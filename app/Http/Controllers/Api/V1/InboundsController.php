<?php

namespace App\Http\Controllers\Api\V1;

use App\Inbound;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreInboundsRequest;
use App\Http\Requests\Admin\UpdateInboundsRequest;
use Yajra\DataTables\DataTables;

class InboundsController extends Controller
{
    public function index()
    {
        return Inbound::all();
    }

    public function show($id)
    {
        return Inbound::findOrFail($id);
    }

    public function update(UpdateInboundsRequest $request, $id)
    {
        $inbound = Inbound::findOrFail($id);
        $inbound->update($request->all());
        

        return $inbound;
    }

    public function store(StoreInboundsRequest $request)
    {
        $inbound = Inbound::create($request->all());
        

        return $inbound;
    }

    public function destroy($id)
    {
        $inbound = Inbound::findOrFail($id);
        $inbound->delete();
        return '';
    }
}
