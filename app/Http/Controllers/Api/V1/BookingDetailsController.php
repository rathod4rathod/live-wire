<?php

namespace App\Http\Controllers\Api\V1;

use App\BookingDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBookingDetailsRequest;
use App\Http\Requests\Admin\UpdateBookingDetailsRequest;
use Yajra\DataTables\DataTables;

class BookingDetailsController extends Controller
{
    public function index()
    {
        return BookingDetail::all();
    }

    public function show($id)
    {
        return BookingDetail::findOrFail($id);
    }

    public function update(UpdateBookingDetailsRequest $request, $id)
    {
        $booking_detail = BookingDetail::findOrFail($id);
        $booking_detail->update($request->all());
        

        return $booking_detail;
    }

    public function store(StoreBookingDetailsRequest $request)
    {
        $booking_detail = BookingDetail::create($request->all());
        

        return $booking_detail;
    }

    public function destroy($id)
    {
        $booking_detail = BookingDetail::findOrFail($id);
        $booking_detail->delete();
        return '';
    }
}
