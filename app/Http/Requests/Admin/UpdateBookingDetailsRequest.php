<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBookingDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'booking_id' => 'required',
            'location' => 'min:3|required',
            'details' => 'min:3|required',
            'date' => 'nullable|date_format:'.config('app.date_format'),
            'time' => 'required|date_format:H:i:s',
        ];
    }
}
