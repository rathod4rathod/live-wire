<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBookingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'date' => 'required|date_format:'.config('app.date_format').' H:i:s',
            'consignment_no' => 'required|unique:bookings,consignment_no,'.$this->route('booking'),
            'consigner_name' => 'min:4|max:25|required',
            'consigner_mobile_no' => 'required',
            'from' => 'required',
            'to' => 'required',
            'consignee_mobile_no' => 'required',
            'service_type' => 'required',
            'weight' => 'max:2147483647|required|numeric',
            'dox' => 'required',
            'amount' => 'max:2147483647|required|numeric',
        ];
    }
}
