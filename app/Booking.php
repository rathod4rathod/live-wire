<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Booking
 *
 * @package App
 * @property string $date
 * @property string $consignment_no
 * @property string $consigner_name
 * @property string $consigner_mobile_no
 * @property string $from
 * @property string $to
 * @property string $consignee_name
 * @property string $consignee_mobile_no
 * @property enum $service_type
 * @property integer $weight
 * @property string $dox
 * @property integer $amount
 * @property string $status
 * @property enum $shipment_status
 * @property string $pod
 * @property text $descvd
*/
class Booking extends Model
{
    use SoftDeletes;

    protected $fillable = ['date', 'consignment_no', 'consigner_name', 'consigner_mobile_no', 'from', 'to', 'consignee_name', 'consignee_mobile_no', 'service_type', 'weight', 'dox', 'amount', 'status', 'shipment_status', 'pod', 'descvd'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Booking::observe(new \App\Observers\UserActionsObserver);
    }

    public static $enum_service_type = ["normal" => "Normal", "priority" => "Priority", "in 6 hrs" => "In 6 hrs"];

    public static $enum_shipment_status = ["shipped" => "Shipped", "in_transit" => "In_transit", "delivered" => "Delivered"];

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDateAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['date'] = Carbon::createFromFormat(config('app.date_format') . ' H:i:s', $input)->format('Y-m-d H:i:s');
        } else {
            $this->attributes['date'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDateAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format') . ' H:i:s');

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $input)->format(config('app.date_format') . ' H:i:s');
        } else {
            return '';
        }
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setWeightAttribute($input)
    {
        $this->attributes['weight'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setAmountAttribute($input)
    {
        $this->attributes['amount'] = $input ? $input : null;
    }
    
}
