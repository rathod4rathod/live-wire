<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Inbound
 *
 * @package App
 * @property string $ref_no
 * @property string $company_name
 * @property enum $type
 * @property string $consignment
 * @property double $weight
 * @property integer $no_of_pieces
 * @property string $remarks
*/
class Inbound extends Model
{
    use SoftDeletes;

    protected $fillable = ['ref_no', 'company_name', 'type', 'consignment', 'weight', 'no_of_pieces', 'remarks'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Inbound::observe(new \App\Observers\UserActionsObserver);
    }

    public static $enum_type = ["DOCUMENT" => "DOCUMENT", "PARCEL" => "PARCEL", "SPECIAL" => "SPECIAL"];

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setWeightAttribute($input)
    {
        if ($input != '') {
            $this->attributes['weight'] = $input;
        } else {
            $this->attributes['weight'] = null;
        }
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setNoOfPiecesAttribute($input)
    {
        $this->attributes['no_of_pieces'] = $input ? $input : null;
    }
    
}
