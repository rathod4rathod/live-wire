<?php

Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {

        Route::resource('inbounds', 'InboundsController', ['except' => ['create', 'edit']]);

        Route::resource('bookings', 'BookingsController', ['except' => ['create', 'edit']]);

        Route::resource('booking_details', 'BookingDetailsController', ['except' => ['create', 'edit']]);

});
