 <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-8">
                    @lang('quickadmin.qa_list')  

                </div>
                <div class="col-md-4">
                    <input type="text" name="search" class="form-control" placeholder="Search" wire:model="search">                    
                </div>
            </div>
        </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('booking_detail_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>@lang('quickadmin.booking-details.fields.booking')</th>
                        <th>@lang('quickadmin.booking-details.fields.location')</th>
                        <th>@lang('quickadmin.booking-details.fields.details')</th>
                        <th>@lang('quickadmin.booking-details.fields.date')</th>
                        <th>@lang('quickadmin.booking-details.fields.time')</th>
                        <th>@lang('Action')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                    @forelse($booking as $data)
                    <tr>
                    	<td>{{$data->id}}</td>
                    	<td>{{$data->booking['consigner_name']}}</td>
                    	<td>{{$data->location}}</td>
                    	<td>{{$data->details}}</td>
                    	<td>{{$data->date}}</td>
                    	<td>{{$data->time}}</td>
                    	<td><button type="button" class="btn btn-primary" wire:click="editBooking({{$data->id}})"> Edit</button>

                    	<a href="#"  class="btn btn-danger"  data-toggle="modal" data-target="#delete_leave{{$data->id}}" > Delete</a></td>
                    	
                    </tr>
                    <!-- Delete Effort Modal -->
                        <!-- Modal -->
                        <div class="modal fade" id="delete_leave{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-header">
                                    <h3>Delete Leave</h3>
                                    <p>Are you sure want to delete?</p>
                                </div>
                              
                            </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" onclick="openDeleteModel({{$data->id}})" wire:click="deleteBooking({{$data->id}})" >Delete</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /Delete Effort Modal -->   

                   @empty
                    <tr>
                        <th colspan="6" style="text-align: center;">No result found</th>                       
                    </tr>
                    @endforelse
                </thead>
            </table>
            Records {{ $booking->firstItem() }} - {{ $booking->lastItem() }} of {{ $booking->total() }} 
            <br>{{ $booking->links() }}

        </div>
    </div>

@push('scripts')
<script type="text/javascript">
	function openDeleteModel(id){         
		$('#delete_leave'+id).modal('hide');
	}
    window.livewire.on('alert', param => {
        toastr[param['type']](param['message'],param['type']);
    });
</script>
@endpush
       