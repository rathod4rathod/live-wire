<div>
     <h3 class="page-title">@lang('quickadmin.booking-details.title')</h3>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <input type="hidden" name="myid"  wire:model="myid">
                <div class="col-xs-4 form-group">
                    <label for="booking_id" class="control-label">Booking id*</label>
                    <select class="form-control select2" required="" id="booking_id" name="booking_id" wire:model="booking_id">
                        <option value=""  selected="selected">Please select</option>
                        @foreach($bookingList as $val)
                            <option value="{{$val->id}}">{{$val->consigner_name}}</option>
                        @endforeach
                    </select>
                    
                        @error('booking_id') <span class="error">{{ $message }}</span> @enderror
                     
                </div>
                <div class="col-xs-4 form-group">
                    <label for="location" class="control-label">Location*</label>
                    <input wire:model="location" class="form-control" placeholder="" required="" name="location" type="text" id="location" >
                   
                        @error('location') <span class="error">{{ $message }}</span> @enderror
                    
                                    </div>
            
                <div class="col-xs-4 form-group">
                    <label for="details" class="control-label">Details*</label>
                    <input wire:model="details" class="form-control" placeholder="" required="" name="details" type="text" id="details">
                   
                        @error('details') <span class="error">{{ $message }}</span> @enderror
                    
                                    </div>
            
                <div class="col-xs-4 form-group">
                    <label for="date" class="control-label">Date*</label>{{$date}}
                    <input wire:model="date" class="form-control date" placeholder="" name="date" type="text" id="date" autocomplete="off">
                   
                        @error('date') <span class="error">{{ $message }}</span> @enderror
                    
                                    </div>
            
                <div class="col-xs-4 form-group">
                    <label for="time" class="control-label">Time*</label>
                    <input wire:model="time" class="form-control timepicker" placeholder="" required="" name="time" type="text" id="time">
                   
                        @error('time') <span class="error">{{ $message }}</span> @enderror
                    
                                    </div>
            </div>
            
  <button class="btn btn-danger" type="button" wire:click="addBooking()">Save</button>
        </div>
    </div>

</div>

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#date').datepicker({
          autoclose: true,          
          todayHighlight: true,
          format: 'dd/mm/yyyy',              
      }).attr('placeholder', 'dd/mm/yyyy');
         $(document).on('change',"#date", function(e){
            @this.set('date',  e.target.value);
         });

       
    });
</script>
@endpush

