@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.inbound.title')</h3>
    @can('inbound_create')
    <p>
        <a href="{{ route('admin.inbounds.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    @can('inbound_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.inbounds.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.inbounds.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('inbound_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('inbound_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.inbound.fields.ref-no')</th>
                        <th>@lang('quickadmin.inbound.fields.company-name')</th>
                        <th>@lang('quickadmin.inbound.fields.type')</th>
                        <th>@lang('quickadmin.inbound.fields.consignment')</th>
                        <th>@lang('quickadmin.inbound.fields.weight')</th>
                        <th>@lang('quickadmin.inbound.fields.no-of-pieces')</th>
                        <th>@lang('quickadmin.inbound.fields.remarks')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('inbound_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.inbounds.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.inbounds.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('inbound_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'ref_no', name: 'ref_no'},
                {data: 'company_name', name: 'company_name'},
                {data: 'type', name: 'type'},
                {data: 'consignment', name: 'consignment'},
                {data: 'weight', name: 'weight'},
                {data: 'no_of_pieces', name: 'no_of_pieces'},
                {data: 'remarks', name: 'remarks'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection