@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.inbound.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.inbound.fields.ref-no')</th>
                            <td field-key='ref_no'>{{ $inbound->ref_no }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.inbound.fields.company-name')</th>
                            <td field-key='company_name'>{{ $inbound->company_name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.inbound.fields.type')</th>
                            <td field-key='type'>{{ $inbound->type }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.inbound.fields.consignment')</th>
                            <td field-key='consignment'>{{ $inbound->consignment }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.inbound.fields.weight')</th>
                            <td field-key='weight'>{{ $inbound->weight }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.inbound.fields.no-of-pieces')</th>
                            <td field-key='no_of_pieces'>{{ $inbound->no_of_pieces }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.inbound.fields.remarks')</th>
                            <td field-key='remarks'>{{ $inbound->remarks }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.inbounds.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop


