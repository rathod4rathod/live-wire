@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.booking.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.booking.fields.date')</th>
                            <td field-key='date'>{{ $booking->date }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.consignment-no')</th>
                            <td field-key='consignment_no'>{{ $booking->consignment_no }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.consigner-name')</th>
                            <td field-key='consigner_name'>{{ $booking->consigner_name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.consigner-mobile-no')</th>
                            <td field-key='consigner_mobile_no'>{{ $booking->consigner_mobile_no }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.from')</th>
                            <td field-key='from'>{{ $booking->from }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.to')</th>
                            <td field-key='to'>{{ $booking->to }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.consignee-name')</th>
                            <td field-key='consignee_name'>{{ $booking->consignee_name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.consignee-mobile-no')</th>
                            <td field-key='consignee_mobile_no'>{{ $booking->consignee_mobile_no }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.service-type')</th>
                            <td field-key='service_type'>{{ $booking->service_type }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.weight')</th>
                            <td field-key='weight'>{{ $booking->weight }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.dox')</th>
                            <td field-key='dox'>{{ $booking->dox }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.amount')</th>
                            <td field-key='amount'>{{ $booking->amount }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.status')</th>
                            <td field-key='status'>{{ $booking->status }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.shipment-status')</th>
                            <td field-key='shipment_status'>{{ $booking->shipment_status }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.pod')</th>
                            <td field-key='pod'>@if($booking->pod)<a href="{{ asset(env('UPLOAD_PATH').'/' . $booking->pod) }}" target="_blank">Download file</a>@endif</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking.fields.descvd')</th>
                            <td field-key='descvd'>{!! $booking->descvd !!}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#booking_details" aria-controls="booking_details" role="tab" data-toggle="tab">Booking details</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="booking_details">
<table class="table table-bordered table-striped {{ count($booking_details) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.booking-details.fields.booking')</th>
                        <th>@lang('quickadmin.booking-details.fields.location')</th>
                        <th>@lang('quickadmin.booking-details.fields.details')</th>
                        <th>@lang('quickadmin.booking-details.fields.date')</th>
                        <th>@lang('quickadmin.booking-details.fields.time')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($booking_details) > 0)
            @foreach ($booking_details as $booking_detail)
                <tr data-entry-id="{{ $booking_detail->id }}">
                    <td field-key='booking'>{{ $booking_detail->booking->consignment_no ?? '' }}</td>
                                <td field-key='location'>{{ $booking_detail->location }}</td>
                                <td field-key='details'>{{ $booking_detail->details }}</td>
                                <td field-key='date'>{{ $booking_detail->date }}</td>
                                <td field-key='time'>{{ $booking_detail->time }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('booking_detail_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.booking_details.restore', $booking_detail->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('booking_detail_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.booking_details.perma_del', $booking_detail->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('booking_detail_view')
                                    <a href="{{ route('admin.booking_details.show',[$booking_detail->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('booking_detail_edit')
                                    <a href="{{ route('admin.booking_details.edit',[$booking_detail->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('booking_detail_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.booking_details.destroy', $booking_detail->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.bookings.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop

@section('javascript')
    @parent
    <script src="//cdn.ckeditor.com/4.5.4/full/ckeditor.js"></script>
    <script>
        $('.editor').each(function () {
                  CKEDITOR.replace($(this).attr('id'),{
                    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
            });
        });
    </script>

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.datetime').datetimepicker({
                format: "{{ config('app.datetime_format_moment') }}",
                locale: "{{ App::getLocale() }}",
                sideBySide: true,
            });
            
        });
    </script>
            
@stop
