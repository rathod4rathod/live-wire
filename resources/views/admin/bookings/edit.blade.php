@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.booking.title')</h3>
    
    {!! Form::model($booking, ['method' => 'PUT', 'route' => ['admin.bookings.update', $booking->id], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('date', trans('quickadmin.booking.fields.date').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('date', old('date'), ['class' => 'form-control datetime', 'placeholder' => 'Date', 'required' => '']) !!}
                    <p class="help-block">Date</p>
                    @if($errors->has('date'))
                        <p class="help-block">
                            {{ $errors->first('date') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('consignment_no', trans('quickadmin.booking.fields.consignment-no').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('consignment_no', old('consignment_no'), ['class' => 'form-control', 'placeholder' => 'Consignment No', 'required' => '']) !!}
                    <p class="help-block">Consignment No</p>
                    @if($errors->has('consignment_no'))
                        <p class="help-block">
                            {{ $errors->first('consignment_no') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('consigner_name', trans('quickadmin.booking.fields.consigner-name').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('consigner_name', old('consigner_name'), ['class' => 'form-control', 'placeholder' => 'Consigner Name', 'required' => '']) !!}
                    <p class="help-block">Consigner Name</p>
                    @if($errors->has('consigner_name'))
                        <p class="help-block">
                            {{ $errors->first('consigner_name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('consigner_mobile_no', trans('quickadmin.booking.fields.consigner-mobile-no').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('consigner_mobile_no', old('consigner_mobile_no'), ['class' => 'form-control', 'placeholder' => 'Consigner Mobile No', 'required' => '']) !!}
                    <p class="help-block">Consigner Mobile No</p>
                    @if($errors->has('consigner_mobile_no'))
                        <p class="help-block">
                            {{ $errors->first('consigner_mobile_no') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('from', trans('quickadmin.booking.fields.from').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('from', old('from'), ['class' => 'form-control', 'placeholder' => 'From', 'required' => '']) !!}
                    <p class="help-block">From</p>
                    @if($errors->has('from'))
                        <p class="help-block">
                            {{ $errors->first('from') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('to', trans('quickadmin.booking.fields.to').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('to', old('to'), ['class' => 'form-control', 'placeholder' => 'To', 'required' => '']) !!}
                    <p class="help-block">To</p>
                    @if($errors->has('to'))
                        <p class="help-block">
                            {{ $errors->first('to') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('consignee_name', trans('quickadmin.booking.fields.consignee-name').'', ['class' => 'control-label']) !!}
                    {!! Form::text('consignee_name', old('consignee_name'), ['class' => 'form-control', 'placeholder' => 'Consignee Name']) !!}
                    <p class="help-block">Consignee Name</p>
                    @if($errors->has('consignee_name'))
                        <p class="help-block">
                            {{ $errors->first('consignee_name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('consignee_mobile_no', trans('quickadmin.booking.fields.consignee-mobile-no').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('consignee_mobile_no', old('consignee_mobile_no'), ['class' => 'form-control', 'placeholder' => 'Consignee Mobile No', 'required' => '']) !!}
                    <p class="help-block">Consignee Mobile No</p>
                    @if($errors->has('consignee_mobile_no'))
                        <p class="help-block">
                            {{ $errors->first('consignee_mobile_no') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('service_type', trans('quickadmin.booking.fields.service-type').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('service_type', $enum_service_type, old('service_type'), ['class' => 'form-control select2', 'required' => '']) !!}
                    <p class="help-block">Service type</p>
                    @if($errors->has('service_type'))
                        <p class="help-block">
                            {{ $errors->first('service_type') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('weight', trans('quickadmin.booking.fields.weight').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('weight', old('weight'), ['class' => 'form-control', 'placeholder' => 'Weight', 'required' => '']) !!}
                    <p class="help-block">Weight</p>
                    @if($errors->has('weight'))
                        <p class="help-block">
                            {{ $errors->first('weight') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('dox', trans('quickadmin.booking.fields.dox').'*', ['class' => 'control-label']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('dox'))
                        <p class="help-block">
                            {{ $errors->first('dox') }}
                        </p>
                    @endif
                    <div>
                        <label>
                            {!! Form::radio('dox', '1', false, ['required' => '']) !!}
                            Dox
                        </label>
                    </div>
                    <div>
                        <label>
                            {!! Form::radio('dox', '2', false, ['required' => '']) !!}
                            Non Dox
                        </label>
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('amount', trans('quickadmin.booking.fields.amount').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('amount', old('amount'), ['class' => 'form-control', 'placeholder' => 'Amount', 'required' => '']) !!}
                    <p class="help-block">Amount</p>
                    @if($errors->has('amount'))
                        <p class="help-block">
                            {{ $errors->first('amount') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('status', trans('quickadmin.booking.fields.status').'', ['class' => 'control-label']) !!}
                    {!! Form::text('status', old('status'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('status'))
                        <p class="help-block">
                            {{ $errors->first('status') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('shipment_status', trans('quickadmin.booking.fields.shipment-status').'', ['class' => 'control-label']) !!}
                    {!! Form::select('shipment_status', $enum_shipment_status, old('shipment_status'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('shipment_status'))
                        <p class="help-block">
                            {{ $errors->first('shipment_status') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('pod', trans('quickadmin.booking.fields.pod').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('pod', old('pod')) !!}
                    @if ($booking->pod)
                        <a href="{{ asset(env('UPLOAD_PATH').'/' . $booking->pod) }}" target="_blank">Download file</a>
                    @endif
                    {!! Form::file('pod', ['class' => 'form-control']) !!}
                    {!! Form::hidden('pod_max_size', 5) !!}
                    <p class="help-block"></p>
                    @if($errors->has('pod'))
                        <p class="help-block">
                            {{ $errors->first('pod') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('descvd', trans('quickadmin.booking.fields.descvd').'', ['class' => 'control-label']) !!}
                    {!! Form::textarea('descvd', old('descvd'), ['class' => 'form-control editor', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('descvd'))
                        <p class="help-block">
                            {{ $errors->first('descvd') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent
    <script src="//cdn.ckeditor.com/4.5.4/full/ckeditor.js"></script>
    <script>
        $('.editor').each(function () {
                  CKEDITOR.replace($(this).attr('id'),{
                    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
            });
        });
    </script>

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.datetime').datetimepicker({
                format: "{{ config('app.datetime_format_moment') }}",
                locale: "{{ App::getLocale() }}",
                sideBySide: true,
            });
            
        });
    </script>
            
@stop