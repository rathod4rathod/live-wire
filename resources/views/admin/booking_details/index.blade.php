@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
   
    @can('booking_detail_create')
    @livewire('add-booking')
    @endcan

    @livewire('booking')
@stop