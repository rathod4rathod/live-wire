@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.booking-details.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.booking-details.fields.booking')</th>
                            <td field-key='booking'>{{ $booking_detail->booking->consignment_no ?? '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking-details.fields.location')</th>
                            <td field-key='location'>{{ $booking_detail->location }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking-details.fields.details')</th>
                            <td field-key='details'>{{ $booking_detail->details }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking-details.fields.date')</th>
                            <td field-key='date'>{{ $booking_detail->date }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.booking-details.fields.time')</th>
                            <td field-key='time'>{{ $booking_detail->time }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.booking_details.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
            $('.timepicker').datetimepicker({
                format: "{{ config('app.time_format_moment') }}",
            });
            
        });
    </script>
            
@stop
